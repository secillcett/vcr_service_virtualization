require 'cucumber'
require 'capybara/cucumber'
require 'net/http'
require 'vcr'
require 'test/unit'

ENV['SSL_CERT_FILE'] = File.join(File.absolute_path('../..', File.dirname(__FILE__)), 'libraries', 'cacert.pem')


VCR.configure do |c|
  c.hook_into :webmock
  c.cassette_library_dir     = 'features/cassettes'
  c.allow_http_connections_when_no_cassette = true
end


# VCR.cucumber_tags do |t|
#   t.tag  '@localhost_request' # uses default record mode since no options are given
#   t.tags '@disallowed_1', '@disallowed_2', :record => :none
#   t.tag  '@vcr', :use_scenario_name => true
# end

